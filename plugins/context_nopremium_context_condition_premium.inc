<?php

/**
 * Trigger context if current node is premium.
 */
define('CONTEXT_NOPREMIUM_VALUE_PREMIUM', 1);

/**
 * Trigger context if current node is not premium.
 */
define('CONTEXT_NOPREMIUM_VALUE_NOT', 2);

/**
 * Trigger context if user can't access premium nodes.
 */
define('CONTEXT_NOPREMIUM_OPTION_NO', 0);

/**
 * Trigger context if user can access premium nodes.
 */
define('CONTEXT_NOPREMIUM_OPTION_YES', 1);

/**
 * Trigger context whether user can or can't access premium nodes.
 */
define('CONTEXT_NOPREMIUM_OPTION_BOTH', 2);

/**
 * Expose the node premium value as a context condition.
 */
class context_nopremium_context_condition_premium extends context_condition {
  function condition_values() {
    return array(
      CONTEXT_NOPREMIUM_VALUE_PREMIUM => t('trigger if content is premium'),
      CONTEXT_NOPREMIUM_VALUE_NOT => t('trigger if content is NOT premium'),
    );
  }

  /**
   * Condition form.
   */
  function condition_form($context) {
    $values = $this->fetch_from_context($context, 'values');
    return array(
      '#title' => $this->title,
      '#options' => $this->condition_values(),
      '#type' => 'radios',
      '#default_value' => (!empty($values)) ? $values[0] : CONTEXT_NOPREMIUM_VALUE_PREMIUM,
    );
  }

  /**
   * Override of condition_form_submit().
   * Trim any identifier padding for non-unique path menu items.
   */
  function condition_form_submit($values) {
    return array($values);
  }

  function options_form($context) {
    $defaults = $this->fetch_from_context($context, 'options');
    return array(
      'nopremium_access' => array(
        '#title' => t('Set if user'),
        '#type' => 'radios',
        '#options' => array(
          CONTEXT_NOPREMIUM_OPTION_NO => t('Can not access premium content'),
          CONTEXT_NOPREMIUM_OPTION_YES => t('Can access premium content'),
          CONTEXT_NOPREMIUM_OPTION_BOTH => t('Whether they can or can\'t access premium content'),
        ),
        '#default_value' => isset($defaults['nopremium_access']) ? $defaults['nopremium_access'] : TRUE,
      ),
    );
  }

  function execute($node) {
    foreach ($this->get_contexts() as $context) {
      $values = $context->conditions['premium']['values'];
      $options = $context->conditions['premium']['options'];
      $access_options_met = TRUE;

      $value = 0;
      if (!empty($values)) {
        $first_value = reset($values);
        if ($first_value == CONTEXT_NOPREMIUM_VALUE_PREMIUM) {
          $value = 1;
        }
      }

      switch ($options['nopremium_access']) {
        case CONTEXT_NOPREMIUM_OPTION_NO:
          $access_options_met = !user_access('view full '. $node->type .' premium content');
          break;
        case CONTEXT_NOPREMIUM_OPTION_YES:
          $access_options_met = user_access('view full '. $node->type .' premium content');
          break;
      }

      if ($node->premium == $value && $access_options_met) {
        $this->condition_met($context);
      }
    }
  }
}
