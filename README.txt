Context Node Option Premium for Context 3.x for Drupal 7.x
-----------------------------------------
Context nopremium provides a condition for detecting the premium status of a node when using the nopremium module.

Installation
------------
1. Context nopremium can be installed like any other Drupal module -- place it in
the sites/all/modules directory for your site and enable it.

Dependencies
------------

* Context (http://drupal.org/project/context)
* Node Option Premium (http://drupal.org/project/nopremium)

Maintainers
-----------
This module was developed by colugo
